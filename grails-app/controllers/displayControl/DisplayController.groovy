package displayControl

import airlines.Airlines
import org.codehaus.groovy.grails.web.json.JSONArray
import org.codehaus.groovy.grails.web.json.JSONObject

class DisplayController {

    def index() {

        /**Defining Airlines Object "airlin"
         * all the information stored in an Airlines class is searched in terms of the Airlines Name
         * Name of Airlines is send as a parameter for searching and displaying
         * */
        def airlin = Airlines.findAllByName(params.name)

        //Creates JSON Object and JSON Array for storing data
        JSONObject globalobj = new JSONObject()
        JSONArray arr = new JSONArray()

        //Applying Enhanced Loop until name of airlin is finished
        for(Airlines a: airlin){
            //creating JSON Object inside loop and then storing into JSON Array
            JSONObject obj = new JSONObject()
            obj.put("Name:",a.name)
            obj.put("Address",a.address)
            obj.put("Branch",a.branches)
            obj.put("Pilots",a.pilots)
            obj.put("Types of Planes",a.planeType)
            obj.put("Routes",a.route)
            obj.put("Authorized Agents",a.agentName)
            arr.add(obj)
        }
        //Array of JSONObject is stored in Global JSON Object
        globalobj.put("Details of Airlines",arr)

        //Rendering JSON Object for display all the data stored in the particular name of the Airlines
        render globalobj

    }
}
