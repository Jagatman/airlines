package airlines

import org.codehaus.groovy.grails.web.json.JSONObject

class InsertDataController {

    def index() {
        /** Creates an Object of class Airlines with multiple Parameters
         * Here input Parameters are:
         * 1)name:params.name
         * 2)address:params.address
         * 3)branches:params.branchlocation
         * 4)pilots:params.pilot
         * 5)planeType:params.plane
         * 6)route:params.route
         * 7)agentName:params.agentname
         * */

        def airln = new Airlines(name:params.name,address:params.address,branches:params.branchlocation,pilots: params.pilot,planeType: params.plane,route: params.route,agentName: params.agentname)

        //all the input parameters are saved in an object "airln" of class Airlines()
        airln.save()

        //Creates JSON Objects obj
        JSONObject obj = new JSONObject()

        //Checks weather the data save is true or false
        boolean check = airln.save(flush: true,failOnError: true)

        if(check){
            obj.put("Data Status","Data insertion Successful ")
        }else{
            obj.put("Data Status","Data insertion Failed")
             }
        //Rendering a JSON Object Displays in JSON Format
        render obj


    }
}
