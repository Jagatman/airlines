package airlines

import javax.servlet.Registration
/**       AUTHOR: JAGATMAN MULGUTHI
 *        COLLEGE: NEPAL COLLEGE OF INFORMATION TECHNOLOGY
 *        DATE:-NOV 12, 2014
 *        E-MAIL:- jagat.mul@gmail.com
 *
 * */

class Airlines {
    //Variable Declaration
    String name
    String address
    //One to many Relation within the Domain class
    static hasMany = [branches: String,pilots:String,planeType:String,route:String,agentName:String]

    //Mapping of the tables and Joining the table with respective table name column name, types, key
    static mapping = {
        hasMany joinTable: [name: 'branchName',key: 'airlinesId',column: 'branchName',type: "text"],
                           [name: 'pilotName',key: 'airlinesId',column: 'pilotName',type: "text"],
                           [name: 'planeType',key: 'airlinesId',column: 'planeType',type: "text"],
                           [name: 'route',    key: 'airlinesId',column: 'route',type: "text"],
                           [name: 'agentName',key: 'airlinesId',column: 'agentName',type: "text"]
    }
    static constraints = {

        name(nullabel:false,blank: false)
        address(nullabel:false,blank: false)
        branches(nullabel:false,blank: false)
        pilots(nullabel:false,blank: false)
        planeType(nullabel:false,blank: false)
        route(nullabel:false,blank: false)
        agentName(nullabel:false,blank: false)
    }

}